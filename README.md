# CS61a

CS61A, UC Berkeley Spring 2019

https://inst.eecs.berkeley.edu/~cs61a/sp20/

The CS 61 series is an introduction to computer science, with particular emphasis on software and on machines from a programmer's point of view.

CS 61A concentrates on the idea of abstraction, allowing the programmer to think in terms appropriate to the problem rather than in low-level operations dictated by the computer hardware.

CS 61A primarily uses the Python 3 programming language, as well as the Scheme programming language and the Structured Query Language (SQL).
